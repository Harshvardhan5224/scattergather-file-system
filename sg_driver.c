////////////////////////////////////////////////////////////////////////////////
//
//  File           : sg_driver.c
//  Description    : This file contains the driver code to be developed by
//                   the students of the 311 class.  See assignment details
//                   for additional information.
//
//   Author        : Harshvardhan Sharma
//   Last Modified : 10/7/20
//

// Include Files
#include <string.h>
#include <stdlib.h>

// Project Includes 
#include "sg_driver.h"
#include "sg_service.h"
#include<sg_cache.h>

// Defines

// Global Data
SgFHandle handle =  3 ;   	//.. 4 File handle 
// Driver file entry

// Global data
int sgDriverInitialized = 0; // The flag indicating the driver initialized
SG_Block_ID sgLocalNodeId;   // The local node identifier

///+  ======== Code to handle Local ID, Remote ID and Seq Nos  =========

///+ Set this to given initila value  ( 10000 )
SG_SeqNum sgLocalSeqno = SG_INITIAL_SEQNO;      // The local sequence number

SG_Node_ID  LocalNode = 0;

///+  Code to maintain List of Remote Node and Seq Nos
struct Node_SeqNo
{
	SG_Node_ID  LocalId;  	// Local Node ID
	SG_Node_ID  remId;    	// rem, NodeId
	SG_SeqNum  	rSeqNo;		// rseq
	struct Node_SeqNo	*next;
};

typedef struct Node_SeqNo  NodeSeqNo ;
NodeSeqNo  *seqRoot=NULL, *seqLast=NULL;

void  AddNodeSeqNo( SG_Node_ID  LocalId, SG_Node_ID remId, SG_SeqNum  rSeqNo );
NodeSeqNo *GetNodeSeqNo(SG_Node_ID  LocalId, SG_Node_ID remId );

///+  Adds s new Remote Seq No in List
void  AddNodeSeqNo( SG_Node_ID  LocalId, SG_Node_ID remId, SG_SeqNum  rSeqNo )
{
	NodeSeqNo *nw;
	
	nw = (NodeSeqNo*) malloc( sizeof(NodeSeqNo));
	nw->LocalId = LocalId;
	nw->remId = remId;
	nw->rSeqNo = rSeqNo;
	nw->next = NULL;
	
	if( seqRoot == NULL )
		seqRoot = nw;
	else
	{
		seqLast->next = nw;
	}
	seqLast = nw;
}

///+   Returns entry for Given Local ID and Remote Node
//  	If not found returns NULL
NodeSeqNo *GetNodeSeqNo(SG_Node_ID  LocalId, SG_Node_ID remId )
{
	NodeSeqNo *ptr = seqRoot;
	while( ptr != NULL )
	{
		if(ptr->LocalId == LocalId && ptr->remId == remId)
		{
			break;
		}
		ptr = ptr->next;
	}
	return ptr;
}


///+ =========Code to handle Seq Nos ends   =========================

// To store the Buffer with data
#pragma pack(1)   // used to remove padding 
typedef struct
{
	uint32_t  Magic_beg;
	SG_Node_ID  SndrID;
	SG_Node_ID  RcvrID;
	SG_Block_ID  BlkID;
	SG_System_OP  Op;
	SG_SeqNum  SndrSeq;
	SG_SeqNum  RcvrSeq;
	char Data;
	char Block[SG_BLOCK_SIZE];
	uint32_t  Magic_end;
} DataBuffer;

// To store the Buffer without data
#pragma pack(1) // used to remove padding
typedef struct
{
	uint32_t  Magic_beg;
	uint64_t  SndrID;
	uint64_t  RcvrID;
	uint64_t  BlkID;
	uint32_t  Op;
	uint16_t  SndrSeq;
	uint16_t  RcvrSeq;
	char Data;
	uint32_t  Magic_end;
} Buffer;
//
// Global Data

// .. data structure for Each block ( stored as Node)
struct DataBlock
{
	SG_Node_ID 	nodeId;   	// node id for the block
	SG_Block_ID blkId;   	// block id for data block
	int 		size;				// length of the data block
	char 		block[SG_BLOCK_SIZE];  		// data block (store as string)
	struct 		DataBlock *next, *prev;	// pointers for linking Blocks
};

typedef struct DataBlock Block;   // type for each Data Block

int blockBeg = 0;    /// to indicate block at 0 0r 512
int seekDone = 0;

// .. data structure to hold file info.
struct File_Info
{
	char 		fname[100];			// store file name (path)
	SgFHandle 	FH;					// file handle ( unique integer) CHECK THIS !!!!!!!!!!!!!!!!
	int 		state;				//opened=1 , closed=0
	size_t  	cur_pos;			// current file pointer position
	size_t  	cur_len; 			// Current length i.e. size of file
	Block 		*blockRoot, *blockEnd;	// List of data blocks (root , end pointer )
	Block		*curBlock;			// refers to current block to refer (works with sgseek)
	struct File_Info	*next;				// pointer for next File
} ;

typedef struct File_Info  FileInfo;
int fId = -1;  // curret file to process
int fCount=0;   // count of opened files

FileInfo  *Files=NULL, *CurFile=NULL, *last=NULL;  // Root and last pointer for list of files

// .. Our function Declarations  ...
int FileReady(SgFHandle);
int AddBlock(char *buf, size_t len, SG_Node_ID  nodeId, SG_Block_ID  blockId);
void updateBlock( char *buf, int len);
Block * GetBlock( size_t offset );
void CleanUp( FileInfo *curFl );
void FileOpen(const char *path );


// Driver support functions
int sgInitEndpoint( void ); // Initialize the endpoint
int sgCreateBlock1( char *data, SG_Node_ID *nodeId, SG_Block_ID *blockId  );
int sgObtainBlock1( char *buf, SG_Node_ID  nodeId, SG_Block_ID  blockId );
int sgUpdateBlock1( char *data, SG_Node_ID  node_id, SG_Block_ID  blk_id  );
int sgStopEndpoint( void );

// .. ...... Our functions  ........
// ..4   Open empty file

//   Open empty file

void FileOpen(const char *path )
{
	FileInfo  *nw;
	nw = (FileInfo*) malloc( sizeof(FileInfo));
	
	strcpy(nw->fname, path );	// save file name
	nw->cur_len = 0;				// pos and  length = 0
	nw->cur_pos = 0;
	nw->state = 1;				// file opened
	nw->blockRoot = NULL;		// root for data blocks
	nw->blockEnd = NULL;
	nw->curBlock = NULL;
	nw->FH = ++handle;			// set a new File handle, CHECK THIS !!!!!!!
	nw->next = NULL;
	if( Files == NULL )
		Files = nw;
	else
		last->next = nw;
		
	last = nw;	
	CurFile = nw;
	fId = fCount;				// new file as current file
	fCount++;
	logMessage(SGDriverLevel, "Opened File [ %s ] with file Handle %d", path, handle );
}

// .. Function to check whether given file is Ready or Not.. 
// Opens file for operations
int FileReady( SgFHandle fh )
{
	int state = 0;
	FileInfo *ptr = Files;
	while( ptr != NULL )
	{
		if ( ptr->FH == fh  && ptr->state == 1  && sgDriverInitialized == 1 )
		{
			state = 1;
			CurFile = ptr;
			break;
		}
		ptr = ptr->next;
	}
	return state;	
}
// ..  ------ FileReady function ends -----


/////  -------  Functions to handle Data Structure for File and Blocks  ------

// .. Function to Add ( i.e. Write) block in the file (data structure) -----
int AddBlock(char *buf, size_t  len, SG_Node_ID  nodeId, SG_Block_ID  blockId)
{
	int i;
	Block *nw;
	nw = (Block*)malloc( sizeof(Block));  // allocate space
	for( i=0; i<len; i++ )		// copy data block
	{
		nw->block[i] = buf[i];
	}
	nw->nodeId = nodeId;	// store Node and Block IDs
	nw->blkId = blockId;
	nw->size = len;
	 //  Node linnking code...
	nw->prev = nw->next = NULL; 
	if( CurFile->blockRoot == NULL )
	{
		CurFile->blockRoot = nw;
	}
	else
	{
		nw->prev = CurFile->blockEnd;
		CurFile->blockEnd->next = nw;
	}
	CurFile->blockEnd = nw;
	CurFile->cur_len += len;	// increase file length
	CurFile->cur_pos += len;	// update current pos. in file
	CurFile->curBlock = nw;  // update the block pointer
	return len;
}
// ..  ------ AddBlock function ends -----


/// --- update existing block  ----
//     update current position in file
void updateBlock( char *buf, int len)
{
	int i, j;
	for(i = blockBeg,j = 0; i<blockBeg+len; i++,j++)
	{
		CurFile->curBlock->block[i] = buf[j];
	}
	CurFile->cur_pos += len;
}

// .. function to reach the given block -----
// Input : offset to seek
// Output : Returns the block pointer for the offset
Block * GetBlock( size_t offset )
{
	int i, blocks;
	Block *blk= CurFile->blockRoot;
	blocks = offset / SG_BLOCK_SIZE;    // .. this can vary ..
	blockBeg = offset % SG_BLOCK_SIZE;
	for(i=1; i <= blocks; i++)  // move at the end of given blocks
	{
		blk = blk->next;
	}
	return blk;
} 
// ..  ------ GetBlock function ends -----


// .. function to clean-up the data structure -----
void CleanUp( FileInfo *curFl )
{
	Block *cur, *prv=NULL;
	cur = curFl->blockRoot->next;
	while( cur!= NULL )
	{
		free(cur->prev);
		prv = cur;
		cur = cur->next;
	}
	free(prv);
	curFl->blockRoot = curFl->blockEnd = NULL;
}
// ..  ------ CleanUp function ends -----

// Functions

//
// File system interface implementation

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgopen
// Description  : Open the file for for reading and writing
//
// Inputs       : path - the path/filename of the file to be read
// Outputs      : file handle if successful test, -1 if failure

SgFHandle sgopen(const char *path) 
{
	//printf("\n === file open starts ===\n");
   // First check to see if we have been initialized
    if (!sgDriverInitialized) 
	{
        // Call the endpoint initialization 
        if ( sgInitEndpoint() )    // SG_INIT-ENDPOINT   operation
		{
            logMessage( LOG_ERROR_LEVEL, "sgopen: Scatter/Gather endpoint initialization failed." );
            return( -1 );
        }
        // Set to initialized
        sgDriverInitialized = 1;   // indicates SG Driver started
        
		/// Initialize cache
		initSGCache(SG_MAX_CACHE_ELEMENTS );
    }

	// FILL IN THE REST OF THE CODE	

	
	FileOpen(path);
    
    //printf("\n === file open ends ===\n");
    // Return the file handle 
    return handle;
}


////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgread
// Description  : Read data from the file
//
// Inputs       : fh - file handle for the file to read from
//                buf - place to put the data
//                len - the length of the read
// Outputs      : number of bytes read, -1 if failure

int sgread(SgFHandle fh, char *buf, size_t len) 
{
	SG_Node_ID   nodeId;
	SG_Block_ID  blkId;
	
	char *b;
	int i, j;

	if( FileReady(fh))
	{	
		if( !seekDone)
			CurFile->curBlock = GetBlock(CurFile->cur_pos); 
		if( CurFile->cur_pos < CurFile->cur_len)
		{
			// get block Id and Node Id from Current Block
			nodeId = CurFile->curBlock->nodeId;
			blkId = CurFile->curBlock->blkId;
			
			// check Cache
			b = getSGDataBlock( nodeId, blkId);
			
			if( b == NULL )  // Not in Cache ,read from file
			{
				sgObtainBlock1( buf, nodeId, blkId);  // SG_OBTAIN_BLOCK   operation
				putSGDataBlock( nodeId, blkId, buf);  // Write new entry in Cache
			}
			else   // Read from cache
			{
				logMessage( SGDriverLevel,"sgDriverObtainBlock: Used cached block");
			}
			for(i= blockBeg,  j=0 ; i<blockBeg+len; i++, j++ )  // Get from file
			{
				buf[j] = CurFile->curBlock->block[i];
			}
			putSGDataBlock( nodeId, blkId, buf);  // Write new entry in Cache
			logMessage( SGDriverLevel, "Read %s (%lu bytes at offset %lu)",CurFile->fname,len,CurFile->cur_pos);			
			
			CurFile->cur_pos += len;		// update file pointer positions
		}
	}
	seekDone = 0;
    // Return the bytes processed
    return( len );    // .. -1 for error ,  0 or more for bytes read
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgwrite
// Description  : write data to the file
//
// Inputs       : fh - file handle for the file to write to
//                buf - pointer to data to write
//                len - the length of the write
// Outputs      : number of bytes written if successful test, -1 if failure


int sgwrite(SgFHandle fh, char *buf, size_t len) 
{
	SG_Node_ID   nwNodeId;   // new Node and Block IDs to be retrieved from SG System
    SG_Block_ID  nwBlkId;
    int i, j;
	char myBuf[1024];
	
	// Put block in proper place according to Offset
	
	FileReady( fh );
	
	if( !seekDone )
		CurFile->curBlock = GetBlock( CurFile->cur_pos);
		
	// update data ( last block)
	if( CurFile->cur_pos == CurFile->cur_len && CurFile->cur_pos % SG_BLOCK_SIZE != 0 )
	{
		//printf("UPDATE LAST BLOCK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		nwNodeId = CurFile->curBlock->nodeId;
		nwBlkId = CurFile->curBlock->blkId;
		
		// Check in Cache
		getSGDataBlock(nwNodeId, nwBlkId);
		logMessage( SGDriverLevel,"sgDriverObtainBlock: Used cached block");  
		
		// copy part of data from file
		for( i=0; i< blockBeg; i++)
			myBuf[i] = CurFile->curBlock->block[i];
			
		// copy data from buf
		for( i=blockBeg, j=0; i < blockBeg + len; i++, j++ )
			myBuf[i] = buf[j];
		
		sgUpdateBlock1( myBuf, nwNodeId, nwBlkId);  // SG Service update function
		
		putSGDataBlock(nwNodeId,nwBlkId, buf);
		
		// update block in file
		updateBlock( buf, len);  // update block and cur file pos
		CurFile->curBlock->size += len;
		CurFile->cur_len += len;
	}
	
	
	// Add data (new block)
	else if( CurFile->cur_pos == CurFile->cur_len && CurFile->cur_pos % SG_BLOCK_SIZE == 0 )
	{
		if( sgCreateBlock1( buf, &nwNodeId, &nwBlkId )  == 0 )  // SG_CREATE_BLOCK  operation
		{
			getSGDataBlock(nwNodeId, nwBlkId);
			AddBlock( buf, len, nwNodeId, nwBlkId );   	// adds new Block at the end
			putSGDataBlock(nwNodeId,nwBlkId, buf);
			logMessage (SGDriverLevel, "sgDriverCreateBlock: Created block [%ul] on node [%ul]", nwBlkId, nwNodeId );
		}
	}
		
	// update in between
	else if( CurFile->cur_pos < CurFile->cur_len )
	{
		nwNodeId = CurFile->curBlock->nodeId;
		nwBlkId = CurFile->curBlock->blkId;
		
		getSGDataBlock( nwNodeId, nwBlkId);
		logMessage( SGDriverLevel,"sgDriverObtainBlock: Used cached block"); 
		
		blockBeg = CurFile->cur_pos % SG_BLOCK_SIZE;
		
		// Fil data in a block to update
		for( i=0; i< blockBeg; i++)
			myBuf[i] = CurFile->curBlock->block[i];
		for( i=blockBeg , j=0; i< blockBeg+len; i++, j++)
			myBuf[i] = buf[j];
		for( i=blockBeg+len; i< SG_BLOCK_SIZE; i++)
			myBuf[i] = CurFile->curBlock->block[i];
			
		sgUpdateBlock1( myBuf, nwNodeId, nwBlkId);
		
		putSGDataBlock(nwNodeId,nwBlkId, buf);
	
		// update file
		updateBlock( buf, len);
	}
	seekDone = 0;
	//logMessage( LOG_INFO_LEVEL, "    .......... Write Done !!!  .........");
    return( len );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgseek
// Description  : Seek to a specific place in the file
//
// Inputs       : fh - the file handle of the file to seek in
//                off - offset within the file to seek to
// Outputs      : new position if successful, -1 if failure

int sgseek(SgFHandle fh, size_t off) 
{
	Block *blk=NULL;
	//int len = 0;
	if( FileReady(fh) )
	{
		blockBeg = 0;
		if( off <= CurFile->cur_len )
		{
			blk = GetBlock( off );
			CurFile->curBlock = blk;		// Current block 
			CurFile->cur_pos = off;		// current offset.
			seekDone = 1;
			return off;
		}
	}
    return( -1 );		// .. ret  -1 or new position
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgclose
// Description  : Close the file
//
// Inputs       : fh - the file handle of the file to close
// Outputs      : 0 if successful test, -1 if failure

int sgclose(SgFHandle fh) 
{
	//logMessage (SGDriverLevel," ........ file Close starts .......");
	if( FileReady(fh))
	{
		CurFile->state = 0;  	// File closed
		CurFile->cur_pos = 0;
		CurFile->cur_len =0;	// file length = 0
		CurFile->curBlock = NULL;
		CleanUp(CurFile);				//clean up data structure
	}
	else
		return -1;  // if file error
		
	//printf("\n === file Close starts ===\n");
    // Return successfully
    return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgshutdown
// Description  : Shut down the filesystem
//
// Inputs       : none
// Outputs      : 0 if successful test, -1 if failure

int sgshutdown(void) 
{
	//logMessage (SGDriverLevel," .......======== SG Shutdown starts =======.......");
	sgStopEndpoint();
	
	///sgclose(Files[fId].FH);   		// Close file
	sgDriverInitialized = 0;  	// driver closed
	
	closeSGCache();  ///
	
    // Log, return successfully
    logMessage( LOG_INFO_LEVEL, "Shut down Scatter/Gather driver." );
    return( 0 );
}


// Functions

//
// File system interface implementation


//
// Functions
////////////////////////////////////////////////////////////////////////////////
//
// Function     : serialize_sg_packet
// Description  : Serialize a ScatterGather packet (create packet)
//
// Inputs       : loc - the local node identifier
//                rem - the remote node identifier
//                blk - the block identifier
//                op - the operation performed/to be performed on block
//                sseq - the sender sequence number
//                rseq - the receiver sequence number
//                data - the data block (of size SG_BLOCK_SIZE) or NULL
//                packet - the buffer to place the data
//                plen - the packet length (int bytes)
// Outputs      : 0 if successfully created, -1 if failure

SG_Packet_Status serialize_sg_packet( SG_Node_ID loc, SG_Node_ID rem, SG_Block_ID blk, 
        SG_System_OP op, SG_SeqNum sseq, SG_SeqNum rseq, char *data, 
        char *packet, size_t *plen ) 
{

	// *packet 

    int i;  // for looping etc
	
	SG_Packet_Status pkt_status= SG_PACKT_OK;   // for getting Status of the Current Package
	Buffer bfr;  								// here we will create the buffer - no data block
	DataBuffer dbfr;  
	

	int dataLen = 0;  					// length actual data block
	int pktLen = 0;  					// Actual packet length


	// Checking for data block is present or Not
	if( data == NULL )
	{
		dataLen = 0;
	}
	else
	{
		dataLen = 1024;
	}
	
	// ** some more errors to handle ??

	//  ============== *** Packet creation with structure   *** ===============
	
	// copy data into proper buffer
	if( dataLen == 0 )   // Buffer without data block
	{
		bfr.Magic_beg = SG_MAGIC_VALUE;
		bfr.SndrID = loc;
		bfr.RcvrID = rem;
		bfr.BlkID = blk;
		bfr.Op = op;
		bfr.SndrSeq = sseq;
		bfr.RcvrSeq = rseq;
		bfr.Data = 0;   // Indicate No data block
		bfr.Magic_end = SG_MAGIC_VALUE;
		pktLen = sizeof(Buffer);    // Actual packet length
		// packet = (char*)&bfr;

	
	}
	else     			// Buffer with Data Block
	{
		dbfr.Magic_beg = SG_MAGIC_VALUE;
		dbfr.SndrID = loc;
		dbfr.RcvrID = rem;
		dbfr.BlkID = blk;
		dbfr.Op = op;
		dbfr.SndrSeq = sseq;
		dbfr.RcvrSeq = rseq;
		dbfr.Data = 1;   // Indcate data block is present
		dbfr.Magic_end = SG_MAGIC_VALUE;
		if (data == NULL )  // data block not present
		{
			pkt_status = SG_PACKT_BLKDT_BAD;
			// logMessage( LOG_INFO_LEVEL, "exiting from packet block data bad\n"); 
			return( pkt_status );
		}
		for(i=0; i<dataLen; i++ )   // copy data
		{
			dbfr.Block[i]= data[i];
		}
		// dbfr.Block[i] = '\0';
		pktLen = sizeof(DataBuffer);   // Actual packet length
		//packet = (char*)&dbfr;
	}
	//--------------------------
	// data into packet:

	if( dataLen == 0)
		memcpy( packet , (&bfr),  pktLen);
	else
		memcpy( packet, (&dbfr), pktLen );

	
	*plen = pktLen;
	//----------- packet creation ends-------
	// Check before storing each value.. if invalid (value should be non zero or certain value)
		
	if( loc == 0 )
	{
		pkt_status = SG_PACKT_LOCID_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from loc\n");
		return( pkt_status );
	}
	else if( rem == 0 )
	{
		pkt_status = SG_PACKT_REMID_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from REM\n");
		return( pkt_status );
	}
	else if( blk == 0 )
	{
		pkt_status = SG_PACKT_BLKID_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from blk\n");
		return( pkt_status );
	}
	else if( op < 0 || op >6 )  // Op code  0 to 6
	{
		pkt_status = SG_PACKT_OPERN_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from op\n");
		return( pkt_status );
	}
	else if( sseq == 0 )
	{
		pkt_status = SG_PACKT_SNDSQ_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from sseq\n");
		return( pkt_status );
	}
	else if( rseq == 0 )
	{
		pkt_status = SG_PACKT_RCVSQ_BAD;
		// logMessage( LOG_INFO_LEVEL, "exiting from rseq\n");
		return( pkt_status );
	}
	
	else if (dataLen == 0 && data != NULL)  // data block courrupted
	{
		pkt_status = SG_PACKT_BLKLN_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from packet block length bad\n");
		return( pkt_status );
	}
	else if( dataLen > 0 && pktLen < 512 )
	{
		pkt_status = SG_PACKT_PDATA_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from packet pdata bad\n");
		return( pkt_status );
	}
	
	//pkt_status = SG_PACKT_OK;
	
    return ( SG_PACKT_OK );   // ** Write the status as given in sg_defs.h enumeration
}
// ------------------ Serialize function Ends ---------------- //


////////////////////////////////////////////////////////////////////////////////
//
// Function     : deserialize_sg_packet
// Description  : De-serialize a ScatterGather packet (unpack packet)
//
// Inputs       : loc - the local node identifier
//                rem - the remote node identifier
//                blk - the block identifier
//                op - the operation performed/to be performed on block
//                sseq - the sender sequence number
//                rseq - the receiver sequence number
//                data - the data block (of size SG_BLOCK_SIZE) or NULL
//                packet - the buffer to place the data
//                plen - the packet length (int bytes)
// Outputs      : 0 if successfully created, -1 if failure

SG_Packet_Status deserialize_sg_packet( SG_Node_ID *loc, SG_Node_ID *rem, SG_Block_ID *blk, 
        SG_System_OP *op, SG_SeqNum *sseq, SG_SeqNum *rseq, char *data, 
        char *packet, size_t plen ) 
{
	// Read data blocks from Packaet and create different IDs and numbers.

	// // checks : later

	// Packet status
	
	Buffer *bfrPtr;   		// pointer for Buffer( no data block)
	DataBuffer *dPtr;		// pointer for Buffer ( with data block)
	uint32_t  mno1, mno2; 	// magic num
	// int dataLen = 0 ;		// Length of data block // not needed
	SG_Packet_Status  pkt_status = SG_PACKT_OK;
	
	//------Read data blocks from Packaet and create different IDs and numbers.  ----
	// create bufer to Read Values from Packet
	
	if( plen == SG_DATA_PACKET_SIZE  )					// --- Packet has Data Block
	{    		 
		//
		dPtr = (DataBuffer*) packet;  	
		*loc = dPtr->SndrID;
		*rem = dPtr->RcvrID;
		*blk = dPtr->BlkID;
		*op = dPtr->Op;
		*sseq = dPtr->SndrSeq;
		*rseq = dPtr->RcvrSeq;
		mno1 = dPtr->Magic_beg;
		mno2 = dPtr->Magic_end;
		memcpy( data,dPtr->Block,1024);

		if (data == NULL)
		{
			pkt_status = SG_PACKT_BLKDT_BAD; 
			//logMessage( LOG_INFO_LEVEL, "exiting from packet block data bad\n");
			return( pkt_status); 
		}
	}
	else
	{
		// 
		bfrPtr = (Buffer*)packet;  	//--- packet has No datablock
		*loc = bfrPtr->SndrID;
		*rem = bfrPtr->RcvrID;
		*blk = bfrPtr->BlkID;
		*op = bfrPtr->Op;
		*sseq = bfrPtr->SndrSeq;
		*rseq = bfrPtr->RcvrSeq;
		mno1 = bfrPtr->Magic_beg;
		mno2 = bfrPtr->Magic_end;

	}

	// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx performing checks xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	if( *loc == 0 )
	{
		pkt_status = SG_PACKT_LOCID_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *loc\n");
		return( pkt_status); 
	}
	else if( *rem == 0 )
	{
		pkt_status = SG_PACKT_REMID_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *rem\n");
		return( pkt_status); 
	}
	else if( *blk == 0 )
	{
		pkt_status = SG_PACKT_BLKID_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *blk\n");
		return( pkt_status); 
	}
	else if( *op < 0 || *op > 6 )
	{
		pkt_status = SG_PACKT_OPERN_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *op\n");
		return( pkt_status); 
	}
	else if( *sseq == 0 )
	{
		pkt_status = SG_PACKT_SNDSQ_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *sseq\n");
		return( pkt_status); 
	}
	else if( *rseq == 0 )
	{
		pkt_status = SG_PACKT_RCVSQ_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from *rseq\n");
		return( pkt_status); 
	}
	
	else if( mno1 != SG_MAGIC_VALUE || mno2 !=SG_MAGIC_VALUE ) 
	{
		pkt_status = SG_PACKT_PDATA_BAD;
		//logMessage( LOG_INFO_LEVEL, "exiting from packet pdata bad \n");
		return( pkt_status); 
	}
    return( SG_PACKT_OK);   // return the Packet status
}

// Driver support functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sgInitEndpoint
// Description  : Initialize the endpoint
//
// Inputs       : none
// Outputs      : 0 if successfull, -1 if failure

int sgInitEndpoint( void ) {

    // Local variables
    char initPacket[SG_BASE_PACKET_SIZE], recvPacket[SG_BASE_PACKET_SIZE];
    size_t pktlen, rpktlen;
    SG_Node_ID loc, rem;
    SG_Block_ID blkid;
    SG_SeqNum sloc, srem;
    SG_System_OP op;
    SG_Packet_Status ret;

    // Local and do some initial setup
    logMessage( LOG_INFO_LEVEL, "Initializing local endpoint ..." );
    sgLocalSeqno = SG_INITIAL_SEQNO;

    // Setup the packet
    pktlen = SG_BASE_PACKET_SIZE;
    if ( (ret = serialize_sg_packet( SG_NODE_UNKNOWN, // Local ID
                                    SG_NODE_UNKNOWN,   // Remote ID
                                    SG_BLOCK_UNKNOWN,  // Block ID
                                    SG_INIT_ENDPOINT,  // Operation
                                    sgLocalSeqno,    // Sender sequence number    ///+
                                    SG_SEQNO_UNKNOWN,  // Receiver sequence number
                                    NULL, initPacket, &pktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgInitEndpoint: failed serialization of packet [%d].", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Serialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					SG_NODE_UNKNOWN, SG_NODE_UNKNOWN, SG_INIT_ENDPOINT, sgLocalSeqno, SG_SEQNO_UNKNOWN );

    // Send the packet
    rpktlen = SG_BASE_PACKET_SIZE;
    if ( sgServicePost(initPacket, &pktlen, recvPacket, &rpktlen) ) {
        logMessage( LOG_ERROR_LEVEL, "sgInitEndpoint: failed packet post" );
        return( -1 );
    }

    // Unpack the recieived data
    if ( (ret = deserialize_sg_packet(&loc, &rem, &blkid, &op, &sloc, 
                                    &srem, NULL, recvPacket, rpktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgInitEndpoint: failed deserialization of packet [%d]", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Deserialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					loc, rem, SG_INIT_ENDPOINT, sgLocalSeqno, srem );
					
    // Sanity check the return value
    if ( loc == SG_NODE_UNKNOWN ) {
        logMessage( LOG_ERROR_LEVEL, "sgInitEndpoint: bad local ID returned [%ul]", loc );
        return( -1 );
    }

    // Set the local node ID, log and return successfully
    sgLocalNodeId = loc;
    logMessage( LOG_INFO_LEVEL, "Completed initialization of node (local node ID %lu", sgLocalNodeId );
    
    ///+    INCREMENT LOCAL Seq No
    sgLocalSeqno++;
    return( 0 );
}



// ..   -------------------- Create Block function ------------------------
// Function is used to Obtian a Block.
// Inputs :  data block and reference for new Node id and Block id.
// Outputs : 0 on success.. also new block and node id , -1 on error
int sgCreateBlock1( char *data, SG_Node_ID *nodeId, SG_Block_ID *blockId  ) 
{
    // Local variables
    char initPacket[SG_DATA_PACKET_SIZE], recvPacket[SG_DATA_PACKET_SIZE];  // * changed
    size_t pktlen, rpktlen;
    SG_Node_ID loc, rem;
    SG_Block_ID blkid;
    SG_SeqNum sloc, srem;
    SG_System_OP op;
    SG_Packet_Status ret;

	///+ Pointer for checking / updating Remote Node list
	NodeSeqNo *ptr =NULL;


    // Setup the packet
    pktlen = SG_DATA_PACKET_SIZE;   // * changed
    if ( (ret = serialize_sg_packet( sgLocalNodeId, // Local ID      // * changed
                                    SG_NODE_UNKNOWN,   // Remote ID 
                                    SG_BLOCK_UNKNOWN,  // Block ID
                                    SG_CREATE_BLOCK,  //  CREATE BLOCK Operation   // * changed
                                    sgLocalSeqno,    // Sender sequence number
                                    SG_SEQNO_UNKNOWN,  // Receiver sequence number
                                     data , initPacket, &pktlen)) != SG_PACKT_OK )   // * changed
	{
        logMessage( LOG_ERROR_LEVEL, "sgCreateBlock: failed serialization of packet [%d].", ret );  // * changed
        return( -1 );
    }


	///+
	logMessage(SGDriverLevel, "Serialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[y]",
					sgLocalNodeId, SG_NODE_UNKNOWN, SG_CREATE_BLOCK, sgLocalSeqno, SG_SEQNO_UNKNOWN );

    // Send the packet
    rpktlen = SG_DATA_PACKET_SIZE;   // * changed
    if ( sgServicePost(initPacket, &pktlen, recvPacket, &rpktlen) ) 
	{
        logMessage( LOG_ERROR_LEVEL, "sgCreateBlock: failed packet post" );
        return( -1 );
    }

    // Unpack the recieived data
    if ( (ret = deserialize_sg_packet(&loc, &rem, &blkid, &op, &sloc, 
                                    &srem, data , recvPacket, rpktlen)) != SG_PACKT_OK ) 
	{
        logMessage( LOG_ERROR_LEVEL, "sgCreateBlock: failed deserialization of packet [%d]", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Deserialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					loc, rem, SG_CREATE_BLOCK, sgLocalSeqno, srem );
					

	///+ Checking Remote Node
	ptr = GetNodeSeqNo(sgLocalNodeId, rem );
	if( ptr == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "sgAddNodeInfo: unable to find node in node table [%ul]", rem );
		AddNodeSeqNo(sgLocalNodeId, rem, srem+1);
		logMessage( SGDriverLevel, "Added node (%ul, seq=%d) to driver node table", rem, srem+1);
		logMessage( SGDriverLevel, "Creating block on unknown node [%ul], in sequence [%d]", rem, srem);
	}
	else
	{
		logMessage( SGDriverLevel, "Creating block on known node [%ul], in sequence [%d]", rem, srem);
		ptr->rSeqNo++;
	}

	//logMessage(LOG_INFO_LEVEL, )

    // Sanity check the return value
    if ( blkid == SG_BLOCK_UNKNOWN ) // * changed
	{
        logMessage( LOG_ERROR_LEVEL, "sgCreateBlock: bad Block ID returned [%ul]", blkid );  // changed
        return( -1 );
    }

    // Set the new Node and Block IDs , log and return successfully
    //sgLocalNodeId = loc;   // * changed
    *nodeId = rem;		// new Node iD and Block ID
    *blockId = blkid;
    //logMessage( LOG_INFO_LEVEL, "Completed creation of new Block ( Block ID %lu)", blkid );
	//logMessage (SGDriverLevel, "sgDriverCreateBlock: Created block [%ul] on node [%ul]", blkid, rem);
    //printf("\nsgDriverCreateBlock: Created block [%lu] on node [%lu]\n", blkid, rem);
    
    ///+    INCREMENT LOCAL Seq No
    sgLocalSeqno++;
	return( 0 );
}
// .. ------------   Create Block function ends  --------------

// ..   -------------------- Obtain Block function ------------------------
// Function is used to Create New Block.
// Inputs :  data block, currnt Node id and Block id.
// Outputs : 0 on success.., -1 on error
int sgObtainBlock1( char *buf, SG_Node_ID  nodeId, SG_Block_ID  blockId ) 
{
    // Local variables
    char initPacket[SG_DATA_PACKET_SIZE], recvPacket[SG_DATA_PACKET_SIZE];
    size_t pktlen, rpktlen;
    SG_Node_ID loc, rem;
    SG_Block_ID blkid;
    SG_SeqNum sloc, srem1, srem2;
    SG_System_OP op;
    SG_Packet_Status ret;

	///+
	NodeSeqNo *ptr;
	ptr = GetNodeSeqNo(sgLocalNodeId, nodeId );  // get Rem Seq no for node
	srem1 = ptr->rSeqNo;
	
    // Setup the packet
    pktlen = SG_BASE_PACKET_SIZE;
    if ( (ret = serialize_sg_packet( sgLocalNodeId, // Local ID   // * changed
                                    nodeId,   // Remote ID		// * changed
                                    blockId,  // Block ID		// *changed
                                    SG_OBTAIN_BLOCK,  // Operation	// * changed
                                    sgLocalSeqno,    // Sender sequence number removed ++
                                    srem1,  // Receiver sequence number   ///+
                                    NULL, initPacket, &pktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgObtainBlock: failed serialization of packet [%d].", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Serialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					sgLocalNodeId, nodeId, SG_OBTAIN_BLOCK, sgLocalSeqno, srem1 );
	
    // Send the packet
    rpktlen = SG_DATA_PACKET_SIZE;
    if ( sgServicePost(initPacket, &pktlen, recvPacket, &rpktlen) ) {
        logMessage( LOG_ERROR_LEVEL, "sgObtainBlock: failed packet post" );
        return( -1 );
    }

    // Unpack the recieived data
    if ( (ret = deserialize_sg_packet(&loc, &rem, &blkid, &op, &sloc, 
                                    &srem2, buf , recvPacket, rpktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgObtainBlock: failed deserialization of packet [%d]", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Deserialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
				loc, rem, SG_OBTAIN_BLOCK, sgLocalSeqno, srem2 );
    
	
	// Sanity check the return value
    if ( loc == SG_NODE_UNKNOWN ) {
        logMessage( LOG_ERROR_LEVEL, "sgObtainBlock: bad local ID returned [%ul]", loc );
        return( -1 );
    }

	///+  Increment the SeqNo for the Node
	ptr->rSeqNo++;
    // logMessage( LOG_INFO_LEVEL, "Completed Obtaining Block Node: %lu, Block: %lu\n", nodeId, blockId );  // * changed
        
	///+    INCREMENT LOCAL Seq No
    sgLocalSeqno++;
	return( 0 );
}
// .. ------------   Obtain block function ends  --------------


// ..  --------------- Update block function ---------------------
int sgUpdateBlock1( char *data, SG_Node_ID  node_id, SG_Block_ID  blk_id  ) 
{
    // Local variables
    char initPacket[SG_DATA_PACKET_SIZE], recvPacket[SG_DATA_PACKET_SIZE];  // * changed
    size_t pktlen, rpktlen;
    SG_Node_ID loc, rem;
    SG_Block_ID blkid;
    SG_SeqNum sloc, srem1, srem2;
    SG_System_OP op;
    SG_Packet_Status ret;
    
    ///+
	NodeSeqNo *ptr;
	ptr = GetNodeSeqNo(sgLocalNodeId, node_id );  // get Rem Seq no for node
	srem1 = ptr->rSeqNo;

    // Setup the packet
    pktlen = SG_DATA_PACKET_SIZE;   // * changed
    if ( (ret = serialize_sg_packet( sgLocalNodeId, // Local ID      // * changed
                                    node_id ,   // Remote ID 
                                    blk_id,  // Block ID
                                    SG_UPDATE_BLOCK,  //  UPDATE BLOCK Operation   // * changed
                                    sgLocalSeqno,    // Sender sequence number
                                    srem1,  // Receiver sequence number
                                     data , initPacket, &pktlen)) != SG_PACKT_OK )   // * changed
	{
        logMessage( LOG_ERROR_LEVEL, "sgUpdateBlock: failed serialization of packet [%d].", ret );  // * changed
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Serialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					sgLocalNodeId, node_id, SG_UPDATE_BLOCK, sgLocalSeqno, srem1 );

    // Send the packet
    rpktlen = SG_DATA_PACKET_SIZE;   // * changed
    if ( sgServicePost(initPacket, &pktlen, recvPacket, &rpktlen) ) 
	{
        logMessage( LOG_ERROR_LEVEL, "sgUpdateBlock: failed packet post" );
        return( -1 );
    }

    // Unpack the recieived data
    if ( (ret = deserialize_sg_packet(&loc, &rem, &blkid, &op, &sloc, 
                                    &srem2, data , recvPacket, rpktlen)) != SG_PACKT_OK ) 
	{
        logMessage( LOG_ERROR_LEVEL, "sgUpdateBlock: failed deserialization of packet [%d]", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Deserialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
				loc, rem, SG_UPDATE_BLOCK, sgLocalSeqno, srem2 );

    // Sanity check the return value
    if ( blkid == SG_BLOCK_UNKNOWN ) // * changed
	{
        logMessage( LOG_ERROR_LEVEL, "sgUpdateBlock: bad Block ID returned [%ul]", blkid );  // changed
        return( -1 );
    }

	

	logMessage (SGDriverLevel, "sgDriverUpdateBlock: Updated block [%ul] on node [%ul]", blk_id, node_id);
    //printf("\nsgDriverCreateBlock: Created block [%lu] on node [%lu]\n", blkid, rem);
    
    ///+
    ptr->rSeqNo++;
    ///+    INCREMENT LOCAL Seq No
    sgLocalSeqno++;
	return( 0 );
}
// .. ------------   Update Block function ends  --------------


// --------------- Function to shutdown the SG system ----------
int sgStopEndpoint( void ) 
{
    // Local variables
    char initPacket[SG_BASE_PACKET_SIZE], recvPacket[SG_BASE_PACKET_SIZE];
    size_t pktlen, rpktlen;
    SG_Node_ID loc, rem;
    SG_Block_ID blkid;
    SG_SeqNum sloc, srem;
    SG_System_OP op;
    SG_Packet_Status ret;

    // Local and do some initial setup
    logMessage( LOG_INFO_LEVEL, "Shuting down local endpoint ..." );
   // sgLocalSeqno = SG_INITIAL_SEQNO;

    // Setup the packet
    pktlen = SG_BASE_PACKET_SIZE;
    if ( (ret = serialize_sg_packet( sgLocalNodeId , // Local ID   // * changed
                                    SG_NODE_UNKNOWN,   // Remote ID
                                    SG_BLOCK_UNKNOWN,  // Block ID
                                    SG_STOP_ENDPOINT,  // Operation
                                    sgLocalSeqno,    // Sender sequence number
                                    SG_SEQNO_UNKNOWN,  // Receiver sequence number
                                    NULL, initPacket, &pktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgStopEndpoint: failed serialization of packet [%d].", ret );
        return( -1 );
    }


	///+
	logMessage(SGDriverLevel, "Serialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					sgLocalNodeId, SG_NODE_UNKNOWN, SG_STOP_ENDPOINT, sgLocalSeqno, SG_SEQNO_UNKNOWN );


    // Send the packet
    rpktlen = SG_BASE_PACKET_SIZE;
    if ( sgServicePost(initPacket, &pktlen, recvPacket, &rpktlen) ) {
        logMessage( LOG_ERROR_LEVEL, "sgStopEndpoint: failed packet post" );
        return( -1 );
    }

    // Unpack the recieived data
    if ( (ret = deserialize_sg_packet(&loc, &rem, &blkid, &op, &sloc, 
                                    &srem, NULL, recvPacket, rpktlen)) != SG_PACKT_OK ) {
        logMessage( LOG_ERROR_LEVEL, "sgStopEndpoint: failed deserialization of packet [%d]", ret );
        return( -1 );
    }

	///+
	logMessage(SGDriverLevel, "Deserialization loc=[%ul], rem=[%ul], op=[%d], sseq=[%d], rseq=[%d], data=[n]",
					sgLocalNodeId, SG_NODE_UNKNOWN, SG_STOP_ENDPOINT, sgLocalSeqno, SG_SEQNO_UNKNOWN );

    // Sanity check the return value
    if ( loc == SG_NODE_UNKNOWN ) {
        logMessage( LOG_ERROR_LEVEL, "sgStopEndpoint: bad local ID returned [%ul]", loc );
        return( -1 );
    }

    // Set the local node ID, log and return successfully
    sgLocalNodeId = loc;
    logMessage( LOG_INFO_LEVEL, "Stopped local node (local node ID %lu)", sgLocalNodeId );
    return( 0 );
}
// .. ------------   StopEndPoint function ends  --------------


/////// ============ The Code in this files ends   =========================        
        
