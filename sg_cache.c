////////////////////////////////////////////////////////////////////////////////
//
//  File           : sg_driver.c
//  Description    : This file contains the driver code to be developed by
//                   the students of the 311 class.  See assignment details
//                   for additional information.
//
//   Author        : YOUR NAME
//   Last Modified : 
//

// Include Files
#include <stdlib.h>
#include <cmpsc311_log.h>
#include<string.h>
// Project Includes
#include <sg_cache.h>

// Defines
/// ------  for Cache operations  ------

int BlockLen = 256;    // Length of one data block for read write

//.. Structure to hold one Cache Entry
struct Cache_Line
{
	int  id;
	SG_Node_ID  node_id;
	SG_Block_ID  block_id;
	char block[SG_BLOCK_SIZE];
	int len;
	struct Cache_Line *prev;
};
typedef struct Cache_Line CacheLine;
// .. Links for Cache D.S.
CacheLine  *top=NULL;

// .. for Cache hits/misses
int searches=0, hits=0, miss=0;
float  hitRatio=0;

int LineCount= 0, lineId = 0 ;
int Bytes=0;


// Functional Prototypes  .... Removed
//void  AddCacheLine( SG_Node_ID nde, SG_Block_ID blk, char *block, int  len) ;
//CacheLine *SearchCacheLine( SG_Node_ID nde, SG_Block_ID blk );
//void DelCacheLine( );

// ------    Casche Data Structure (stack) functions ----

// .. add new block in the DS
void  AddCacheLine( SG_Node_ID nde, SG_Block_ID blk, char *block) 
{
	int i;
	CacheLine *nw;
	// printf("\n === AddCacheline len =%d block=%s===\n", len, block);
	nw = (CacheLine*) malloc( sizeof(CacheLine));
	lineId++;
	LineCount++;
	nw->id = lineId;
	nw->block_id = blk;
	nw->node_id = nde;
	nw->len = BlockLen;
	// copy block
	for (i = 0; i < BlockLen; i++ )
		nw->block[i] = block[i];
	
	nw->prev = top;
	top = nw;
	
	logMessage(LOG_INFO_LEVEL, "Cache state [ %d items, %d bytes used]", LineCount, LineCount*SG_BLOCK_SIZE );
	logMessage(LOG_INFO_LEVEL, "Added cache item Id = %d , length %d", LineCount, SG_BLOCK_SIZE);
}


void UpdateCacheLine(SG_Node_ID nde, SG_Block_ID blk, char *block)
{
	CacheLine *ptr = top;
	int found = 0;
	int i, j;
	while( ptr != NULL )
	{
		if( ptr->node_id == nde && ptr->block_id == blk )
		{
			found = 1;
			if( ptr->len < SG_BLOCK_SIZE)
			{
				for( i=ptr->len, j=0; i<ptr->len+BlockLen; i++, j++)
				{
					ptr->block[i] = block[j];
				}
				ptr->len += BlockLen;
			}
			break;
		}
		ptr = ptr->prev;
	}
	if( !found )
		AddCacheLine(nde, blk, block);
}


// .. Find the Cache Entry in Cache
CacheLine* SearchCacheLine( SG_Node_ID nde, SG_Block_ID blk)
{
	CacheLine *ptr = top;
	searches++;		// Increment cache seach count
	while( ptr != NULL )
	{
		if( ptr->block_id == blk && ptr->node_id ==nde)
		{	
			hits++;  // Cache hit.. Increment hit count
			logMessage(LOG_INFO_LEVEL, "Getting Found cache item %d length =%d",ptr->id, SG_BLOCK_SIZE);	
			break;
		}
		ptr = ptr->prev;
	}
	if( ptr == NULL )
		logMessage(LOG_INFO_LEVEL, "Getting cache item ( Not found ! )");
    // Return pointer or NULL
    return( ptr );
}

// .. delete last cache entry from DS
void DelCacheLine()
{
	CacheLine *ptr = top;
	logMessage(LOG_INFO_LEVEL, "Ejecting cache item ID %d, length %d", ptr->id, SG_BLOCK_SIZE);
	top = top->prev;
	free(ptr);
	LineCount--;
	logMessage(LOG_INFO_LEVEL, "Cache state [ %d items, %d bytes used]", LineCount, LineCount*SG_BLOCK_SIZE );
}


// Functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : initSGCache
// Description  : Initialize the cache of block elements
//
// Inputs       : maxElements - maximum number of elements allowed
// Outputs      : 0 if successful, -1 if failure

int initSGCache( uint16_t maxElements ) 
{
	if( LineCount == 0 )
	{
		top =  NULL;
		LineCount = 0;
		hits =0;
		miss =0;
		lineId = 0;  
		hitRatio = 0;
		logMessage( LOG_INFO_LEVEL,"init_cmpsc311_cache: initialization complete [128/131072]");
		logMessage( LOG_INFO_LEVEL,"Cache state [%d items, %d bytes used]", LineCount, Bytes);
    	// Return successfully
    	return( 0 );
	}
    else 
        return( -1);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : closeSGCache
// Description  : Close the cache of block elements, clean up remaining data
//
// Inputs       : none
// Outputs      : 0 if successful, -1 if failure

//  ... Delete all cashe lines and deallocate Cache
int closeSGCache( void ) 
{
	CacheLine *nxt, *cur;
	hitRatio = (float) hits / searches;
	logMessage( SGDriverLevel, "Closing cache: %d queries, %d hits (%5.2f hit rate)." , searches, hits, hitRatio*100);
	
	cur = top;
	while( cur != NULL )
	{
		nxt = cur->prev;
		free(cur);
		cur = nxt;
	}
	
	top = NULL;
	LineCount = 0;
	logMessage( LOG_INFO_LEVEL, "Closed cmpsc311 cache, deleting 32 items");
    // Return successfully
    return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getSGDataBlock
// Description  : Get the data block from the block cache
//
// Inputs       : nde - node ID to find
//                blk - block ID to find
// Outputs      : pointer to block or NULL if not found

char * getSGDataBlock( SG_Node_ID nde, SG_Block_ID blk ) 
{
	CacheLine *ptr =SearchCacheLine( nde, blk );
	// printf("\n === getDataBlock ===\n");
	if( ptr != NULL ) 
	{
		return ptr->block;
	}
	else
	{
		return NULL;
	}
}


////////////////////////////////////////////////////////////////////////////////
//
// Function     : putSGDataBlock
// Description  : Get the data block from the block cache
//
// Inputs       : nde - node ID to find
//                blk - block ID to find
//                block - block to insert into cache
// Outputs      : 0 if successful, -1 if failure

int putSGDataBlock( SG_Node_ID nde, SG_Block_ID blk, char *block ) 
{
	if( LineCount == SG_MAX_CACHE_ELEMENTS )
		DelCacheLine();

	UpdateCacheLine( nde, blk, block);
	return 0;
}
